## Description
This telegram bot:
1. Saves photos to disk in the image folder if they have faces. 
2. Saves voice messages to the database by user_id.
3. Transform voice messages to .wav 16kHz format and saves it in the voices folder.

## Example
![picture](screenshots/t1.jpg)