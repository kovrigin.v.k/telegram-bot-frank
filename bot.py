import logging
import config
import cv2
import ffmpeg
import numpy as np
import sqlite3
import os

from io import BytesIO
from datetime import datetime
from aiogram import Bot, Dispatcher, executor, types


logging.basicConfig(level=logging.INFO)

bot = Bot(token=config.TOKEN)
dp = Dispatcher(bot)
face_cascade = cv2.CascadeClassifier('models/haarcascade_frontalface_default.xml')


@dp.message_handler(commands=['start', 'help'])
async def handle_start_help(message: types.Message):
    await bot.send_message(message.from_user.id, 
                          "Hello! I take photos with faces and all voice messages!")


@dp.message_handler()
async def echo(message: types.Message):
    await bot.send_message(message.from_user.id, message.text)


@dp.message_handler(content_types=['voice'])
async def handle_voice(message: types.Message):
    """ 
    1. Store voice message into 'voices.db' database
    2. Transform voice message (.ogg) to .wav 16kHz and store it to 'voices/user_id' directory
    """

    await bot.send_message(message.from_user.id, 'I took this voice message for myself')

    # creating special directory for each user
    directory = os.path.dirname(__file__) + '/voices/' + str(message.from_user.id)
    if not os.path.exists(directory): 
        os.mkdir(directory)

    # creating unique filename for voice file
    src_filename = '{0}/{1}.ogg'.format(directory, datetime.timestamp(message.date))
    dest_filename = '{0}/{1}.wav'.format(directory, datetime.timestamp(message.date))

    # saving voice into database
    file = await bot.download_file_by_id(message.voice.file_id)
    byte_file = file.read()
    save_voice_to_db(message.from_user.id, byte_file)

    # transform voice file to .wav
    with open(src_filename, 'wb') as f:
        f.write(byte_file)

    ffmpeg.input(src_filename).output(dest_filename, ar='16k').run()
    os.remove(src_filename)


def save_voice_to_db(user_id: int, file: bytes):
    """
    1. Сreates a database if it doesn't exist
    2. Saves user_id, file to the database
    :param user_id: int
    :param file: bytes
    """
    db = sqlite3.connect('voices.db')
    cursor = db.cursor()

    cursor.execute("""CREATE TABLE IF NOT EXISTS voices (
        user_id INTEGER,
        audio BLOB
    )""")
    db.commit()

    cursor.execute("INSERT INTO voices VALUES (?, ?)", (user_id, file))
    db.commit()

    cursor.close()


@dp.message_handler(content_types=['photo'])
async def handle_photo(message: types.Message):
    """
    Saves photos to disk if they have faces. Uses Haar cascade method
    """
    file = await bot.download_file_by_id(message.photo[-1].file_id)

    img = cv2.imdecode(np.frombuffer(file.read(), np.uint8), 1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

    if len(faces) > 0:
        await message.photo[-1].download('images/{0}.jpg'.format(datetime.timestamp(message.date)))
        await bot.send_message(message.from_user.id, 'I took this photo for myself')
    else:
        await bot.send_message(message.from_user.id, "I don't see faces")
    

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)